**PLEASE READ snippets** :  https://gitlab.com/Ujiki.oO/error-state-javascript-provided-by-wordpress/snippets

https://wordpress.org/support/topic/syntax-check-result-in-yuicompressor/?
https://wordpress.org/support/topic/syntax-check-result-in-yuicompressor-2/?

We have reported that there is an error in JavaScript provided by WordPress above, but this has increased or decreased due to the version upgrade of WordPress. It is the beginning that I reported on my blog.

http://motpresse.votre.space/htaccess/#post-1580

There is also a bash script update, so we will report here. Finally, I hope that JavaScript and diets with YUICOMPRESSOR will be updated, as well as gzip-compressed Java Sctipt.

_/_/_/_/_/_/_/_/_/_/_/
日本語：

https://gitlab.com/Ujiki.oO/error-state-javascript-provided-by-wordpress/snippets
**最新WordPressでのレポート状況はsnippetsにあります。**

WordPress利用者しか興味は無いと思いますが、wordpress.orgで２つの質問をしましたが、自動バージョンアップを経てエラー状況は増減しています。
http://motpresse.votre.space/htaccess/#post-1580
ブログでレポートしましたが、２段階圧縮化によるWordPressの高速化にあたって、
1. JavaScript と StyleSheet を、YUICOMPRESSOR で贅肉を除去し、
2. Gzipで圧縮したファイルを自分のWordPressサイトにアップロードし、
3. .htaccess を定義する。

JavaScript も StyleSheet も、ソースは明確なファイル名で配布するべきで、肥大化傾向になるソースを配布するのは即刻停止して欲しいし、
自動的に配布される JavaScript と StyleSheet は、YUICOMPRESSOR で事前にダイエットされているファイル群にすること。
WordPressオーナーが希望すれば、Gzip圧縮化済みのファイル群も自動的に配布されるべきです。

新たなファイル名：
    abc2019.js
        YUICOMPRESSOR でダイエット処理済み
    abc2019.js.gz
        Gzip 圧縮済み
    abc2019-source.js
        著作権利などコメントもりだくさんのソースファイル。ソースファイル群は自動的に配布されない。