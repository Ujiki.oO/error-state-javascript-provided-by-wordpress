#!/bin/sh
#Version:1.0 2019/01/01
#Version:1.1 2019/01/02
#Version:1.2 2019/01/04
#Version:1.3 2019/01/16
#Version:1.4 2019/02/04
#Version:1.4a 2019/03/12
#Version:1.4b 2020/03/01
#Version:1.4c 2020/03/17
#Version:1.5 2020/03/28
#Version:1.5b 2020/03/30
#Version:1.5c 2020/04/01
#Version:1.5d 2020/04/02
#Version:1.6 2020/04/05
#Version:1.7 2020/04/10
#Version:1.7a 2020/05/06
#Version:1.8 2020/05/27
#Version:1.8a 2020/06/07
#Version:1.8b 2020/07/29
#Version:1.8c 2020/08/14
#Version:1.9 2020/09/06
#Version:2.0 2021/03/10
#Version:2.0a 2021/05/21
#Version:2.0b 2021/09/04
#Version:2.1 2022/07/15
#Version:2.1a 2022/08/05
#Version:2.2 2022/08/11
#Version:2.2a 2022/08/31
VERSION="Version:2.3 2022/11/04 by Ujiki.oO http://fs4y.com/MakeGZifNewer.sh"

LINE='_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/'

echo ${0} $VERSION

REMOVEPERCENT=99
LOGDIR="z  My Special Data"
if [ "`which awk 2>/dev/null`" = "" ]
then	if [ "`which gawk 2>/dev/null`" = "" ]
	then	echo "＞	エラー	あなたのシステムではａｗｋが利用できません"
		echo "Error: awk is not available on your system!"
		exit
	else	AWK=gawk
	fi
else	AWK=awk
fi

LANG=C
if [ ! -d "$LOGDIR" ]
then	mkdir "$LOGDIR"
	if [ $? -ne 0 ] ; then echo "ERROR 000" ; exit ; fi
fi
if [ ! -f "${LOGDIR}/.IGNOREPATH" ]
then	echo "/z  Sources4PHP/" > "${LOGDIR}/.IGNOREPATH"
	touch -t `date +%Y`01010000 "${LOGDIR}/.IGNOREPATH"
fi
STAMP=`date +%Y%m%d%H%M`
LOGFILE="${LOGDIR}/${STAMP}.log"
if [ -f "${LOGFILE}" ]
then	echo "ERROR 001" ; exit
fi
LOGFILE2="${LOGDIR}/${STAMP} ERRORbyTimeStamp.log"
if [ -f "${LOGFILE2}" ]
then	echo "ERROR 002" ; exit
fi
LOGFILE12="${LOGDIR}/${STAMP} MakedMD5forJS.log"
if [ -f "${LOGFILE12}" ]
then	echo "ERROR 1002" ; exit
fi
LOGFILE13="${LOGDIR}/${STAMP} MakedMD5forCSS.log"
if [ -f "${LOGFILE13}" ]
then	echo "ERROR 1003" ; exit
fi
LOGFILE14="${LOGDIR}/${STAMP} MakedGZ.log"
if [ -f "${LOGFILE14}" ]
then	echo "ERROR 1004" ; exit
fi
LOGFILE3="${LOGDIR}/${STAMP} ERRORbyJS.log"
if [ -f "${LOGFILE3}" ]
then	echo "ERROR 003" ; exit
fi
LOGFILE4="${LOGDIR}/${STAMP} ERRORbyCSS.log"
if [ -f "${LOGFILE4}" ]
then	echo "ERROR 004" ; exit
fi
MD5TABLEFILE="${LOGDIR}/${STAMP} MD5TABLE.log"
if [ -f "${MD5TABLEFILE}" ]
then	echo "ERROR 005" ; exit
fi
MD5TABLEFILE2="${LOGDIR}/MD5TABLE.log"
if [ ! -f "${MD5TABLEFILE2}" ]
then	touch -t 200001010000 "${MD5TABLEFILE2}"
	T1ST="y"
fi
MAINTENANCEOFMISSINGDIR="${LOGDIR}/MOMFS"
if [ ! -d "$MAINTENANCEOFMISSINGDIR" ] ; then mkdir "$MAINTENANCEOFMISSINGDIR" ; fi
MAINTENANCEOFMISSINGFILES="${LOGDIR}/MOMFS/${STAMP} MOMFS.log"
if [ -f "${MAINTENANCEOFMISSINGFILES}" ]
then	echo "ERROR 006" ; exit
fi
LINE=' ----------'
BEGINT=`date`
INF=$'\n'
function GETDATETIME(){
	OK_="n"
	if [ "$1" = "" ]
	then	echo "ERROR GETDATETIME() 001 -----"
		exit
	fi
	if [ -f "$1" ]
	then
		A_=`ls --full-time "$1" |$AWK '{printf("%s %s +next minute",$6,$7)}'`
		OK_="y"
		return
	fi
	echo "$1"
}
function GETNUM(){
	if [ "$1" != "" ] ; then NUM2019=$1 ; fi
	if [ "$NUM2019" = "" ] ; then NUM2019=0 ; fi
	NUM2019=`expr $NUM2019 + 1`
	NUM2019=`echo $NUM2019 | $AWK '{printf("%03d",$1)}'`
}
function MAKEGZ(){
	if [ "$1" = "" ] ; then echo "ERROR MAKEGZ() 001" ; exit ; fi
	if [ ! -f "$1" ] ; then echo "ERROR MAKEGZ() 002" ; exit ; fi
	ORGSIZE=`ls -l "$1" | $AWK '{print $5}'`
	if [ $ORGSIZE -le 1000 ]
	then	if [ -f "$1".gz ]
		then	\rm -f "$1".gz
			if [ $? -ne 0 ] ; then echo "ERROR 9999" ; exit ; fi
			echo "rm -f ${1}.gz" | tee -a "$LOGFILE14"
		fi
	else	if [ ! -f "$1".gz ]
		then	gzip -9 -f -k -v "$1"
			if [ $? -ne 0 ] ; then echo "ERROR 9998" ; exit ; fi
			GZSIZE=`ls -l "$1".gz | $AWK '{print $5}'`
			if [ $? -ne 0 ] ; then echo "ERROR 9997" ; exit ; fi
			WARIAI=`expr $GZSIZE \* 100`
			if [ $? -ne 0 ] ; then echo "ERROR 9996" ; exit ; fi
			WARIAI=`expr $WARIAI / $ORGSIZE`
			if [ $? -ne 0 ] ; then echo "ERROR 9995" ; exit ; fi
			echo "gzip -9 -f -k -v $1	${WARIAI}/$GZSIZE" | tee -a "$LOGFILE14"
			if [ $WARIAI -gt 70 ]
			then	\rm -f "$1".gz
				if [ $? -ne 0 ] ; then echo "ERROR 9994" ; exit ; fi
				echo "	-gt 70% : rm -f "$1".gz" | tee -a "$LOGFILE14"
			fi
		else	NEWER=`find "$1" -newer "$1".gz`
			if [ $? -ne 0 ] ; then echo "ERROR 9989" ; exit ; fi
			if [ "$NEWER" != "" ]
			then	echo "	remove ${1}.gz because TIMESTAMP ERROR!" | tee -a "$LOGFILE14"
				\rm -f "$1".gz
				if [ $? -ne 0 ] ; then echo "ERROR 9988" ; exit ; fi
				gzip -9 -f -k -v "$1"
				if [ $? -ne 0 ] ; then echo "ERROR 9987" ; exit ; fi
				echo "	remake ${1}.gz" | tee -a "$LOGFILE14"
			fi
		fi
	fi
}

echo -n "CHECKING SIZE ZERO MD5 FILES ....."
if [ `find -type f -name "*.MD5" -size 0 -print | wc -l` -gt 0 ]
then	echo -n " REMOVED $MD5TABLEFILE2 ....."
	rm -f "${MD5TABLEFILE2}" ; touch -t 202001010000 "${MD5TABLEFILE2}"
	echo " done. And REMOVING ..... find -type f -name \"\*.MD5\" -size 0 -print -exec rm -f \"{}\" \\;"
	find -type f -name "*.MD5" -size 0 -print -exec rm -f "{}" \;
fi
echo " done."

echo -n 'for A in find -type f -name "*[\.\-]min.js" ; do wc -l $A ;done > ${LOGDIR}/COM_JS_WC.TXT	..... '
echo -n "" > "${LOGDIR}/COM_JS_WC.TXT"
for A in `find -type f -name "*[\.\-]min.js" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
do	wc -l "$A" >> "${LOGDIR}/COM_JS_WC.TXT"
done
echo -n 'done.
for A in find -type f -name "*[\.\-]min.css" ; do wc -l $A ;done > ${LOGDIR}/COM_CSS_WC.TXT	..... '
echo -n "" > "${LOGDIR}/COM_CSS_WC.TXT"
for A in `find -type f -name "*[\.\-]min.css" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
do	wc -l "$A" >> "${LOGDIR}/COM_CSS_WC.TXT"
done
echo " done.
"

echo -n "010 Q : If there is no min.js|min.css file, delete min.js.gz|min.css.gz file ? (y/n) [n] : "
read yn
if [ "$yn" = "y" ]
then	for N in `find -type f -name "*[\-\.]min\.js.gz" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	echo -n ">"
		ORGNAME=`echo $N | sed 's/\.gz$//'`
		if [ ! -f "$ORGNAME" ]
		then	rm -f "$N"
			echo -n "
- ${N}	"
		fi
	done
	for N in `find -type f -name "*[\-\.]min\.css.gz" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	echo -n "<"
		ORGNAME=`echo $N | sed 's/\.gz$//'`
		if [ ! -f "$ORGNAME" ]
		then	rm -f "$N"
			echo -n "
- ${N}	"
		fi
	done
	echo ""
fi

echo -n "020 Q : If there is no .js|.css source file, copy from .min. file ? (y/n) [n] : "
read yn
if [ "$yn" = "y" ]
then
	for N in `find -type f -name "*[\-\.]min\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	ORGNAME=`echo $N | sed 's/[\-\.]min\.js$/.js/'`
		OTHERNAME=`echo $ORGNAME | sed 's/\.js$/.min.js/'`
		echo -n ">"
		if [ ! -f "$ORGNAME" ]
		then	echo "NOT FOUND A SOURCE FILE $ORGNAME, making it from $N"
			echo -n "	"
			cp -pv "$N" "$ORGNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
			NN=`basename $ORGNAME`
			DN=`dirname $ORGNAME`
			if [ ! -d "${MAINTENANCEOFMISSINGDIR}/$DN" ]
			then	mkdir -p "${MAINTENANCEOFMISSINGDIR}/$DN"
			fi
			md5sum "$ORGNAME" > "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
			A_=`ls --full-time "$ORGNAME" | $AWK '{printf("%s %s",$6,$7)}'`
			touch -d "$A_" "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
			if [ ! -f "$OTHERNAME" ]
			then	cp -pv "$N" "$OTHERNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
			fi
		fi
	done
	for N in `find -type f -name "*[\-\.]min\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	ORGNAME=`echo $N | sed 's/[\-\.]min\.css$/.css/'`
		OTHERNAME=`echo $ORGNAME | sed 's/\.css$/.min.css/'`
		echo -n "<"
		if [ ! -f "$ORGNAME" ]
		then	echo "NOT FOUND A SOURCE FILE $ORGNAME, making it from $N"
			echo -n "	"
			cp -pv "$N" "$ORGNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
			NN=`basename $ORGNAME`
			DN=`dirname $ORGNAME`
			if [ ! -d "${MAINTENANCEOFMISSINGDIR}/$DN" ]
			then	mkdir -p "${MAINTENANCEOFMISSINGDIR}/$DN"
			fi
			md5sum "$ORGNAME" > "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
			A_=`ls --full-time "$ORGNAME" | $AWK '{printf("%s %s",$6,$7)}'`
			touch -d "$A_" "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
			if [ ! -f "$OTHERNAME" ]
			then	cp -pv "$N" "$OTHERNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
			fi
		fi
	done
	echo ""
	if [ -f "${LOGDIR}/MOMFS.log" ]
	then	cp -p "${LOGDIR}/MOMFS.log" "${MAINTENANCEOFMISSINGDIR}/20200101 MOMFS.log"
		if [ $? -ne 0 ] ; then echo "ERROR9870" ; exit ; fi
		rm -f "${LOGDIR}/MOMFS.log"
	fi
	for N in `find "${LOGDIR}/" -maxdepth 1 -type f -name "* MOMFS\.log"`
	do	if [ -f "$N" ] ; then
		echo -n "	$N ....."
		cp -p "$N" "${MAINTENANCEOFMISSINGDIR}/"
		if [ $? -ne 0 ] ; then echo "ERROR9876" ; exit ; fi
		rm -f "$N"
		echo " done."
		fi
	done
NUM_=0
	if [ `find "${MAINTENANCEOFMISSINGDIR}/" -type f -name "* MOMFS.log" -print | wc -l` -gt 0 ]
	then	for N in `cat "${MAINTENANCEOFMISSINGDIR}"/*\ MOMFS.log | sort -u | $AWK -v A=\' 'BEGIN{FS=A}{print $4}'`
		do	OTHER=`echo $N | sed 's/\.js$/.min.js/' | sed 's/\.css$/.min.css/'`
			OTHER2=`echo $N | sed 's/\.js$/-min.js/' | sed 's/\.css$/-min.css/'`
			NUM_=`expr $NUM_ + 1`
		if [ -f "$N" ] ; then
			if [ -f "${OTHER}.gz" ]
			then	echo -n "${NUM_}A: "
				ls -l "${OTHER}.gz"
			elif [ -f "${OTHER2}.gz" ]
			then	echo -n "${NUM_}A: "
				ls -l "${OTHER2}.gz"
			fi
			echo -n "${NUM_}B: "
			if [ -f "$OTHER" ]
			then	ls -l "$OTHER"
			elif [ 	-f "$OTHER2" ]
			then	ls -l "$OTHER2"
			else	echo "!! NOTHING $OTHER"
			fi
			echo -n "${NUM_}C: "
			ls -l "$N"
			NN=`basename $N`
			DN=`dirname $N`
			if [ ! -d "${MAINTENANCEOFMISSINGDIR}/$DN" ]
			then	mkdir -p "${MAINTENANCEOFMISSINGDIR}/$DN"
			fi
			if [ ! -s "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5" ]
			then
				md5sum "$N" > "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
				A_=`ls --full-time "$N" | $AWK '{printf("%s %s",$6,$7)}'`
				touch -d "$A_" "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
			fi
			echo -n "${NUM_}D: "
			ls -l "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5" | sed 's%/\./%/%'
			echo -n "${LINE}   Please Enter!	KEY \"e\" Enter to SKIP.	"
			read yn
			if [ "$yn" = "e" ] ; then break ; fi
		else	echo "!!! NOTHING $N"
		fi
		done
	fi
fi
echo -n "030 Q : Do you want to REMAKE & RETOUCH MD5 at ${MAINTENANCEOFMISSINGDIR}/${DN} ? (y/n) [n] : "
read yn
if [ "$yn" = "y" ]
then	if [ `find "${MAINTENANCEOFMISSINGDIR}/" -type f -name "* MOMFS.log" -print | wc -l` -gt 0 ]
	then	NUM_=0
		for N in `cat "${MAINTENANCEOFMISSINGDIR}"/*\ MOMFS.log | sort -u | $AWK -v A=\' 'BEGIN{FS=A}{print $4}'`
		do	if [ -f "$N" ]
			then	NN=`basename $N`
				DN=`dirname $N`
				if [ -f "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5" ]
				then	A_=`ls --full-time "$N" | $AWK '{printf("%s %s",$6,$7)}'`
					B_=`head -1 "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5" | md5sum -c | grep OK$`
					if [ "$B_" != "" ]
					then	touch -d "$A_" "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
						NUM_=`expr $NUM_ + 1`
						echo -n "${NUM_} "
						ls -lh "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5" | sed 's%/\./%/%'
					else	echo -n "MD5 CHECK ERROR ! ${N} ... Remake [y/n](n) :"
						read yn
						if [ "$yn" = "y" ]
						then	echo "md5sum $N > ${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.MD5"
							md5sum "$N" > "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
							echo "touch -d $A_ ${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.MD5"
							touch -d "$A_" "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.md5"
						fi
					fi
				fi
			fi
		done
	fi
fi

echo -n "040 Q : DELETE TOP LINE /*! This file is auto-generated */ ? (y/n) [n] : "
read yn
if [ "$yn" = "y" ]
then	for A in `grep -v "^0" "${LOGDIR}/COM_JS_WC.TXT" | sed 's/[^ ]* //'`
	do	echo -n ">"
		if [ "`head -1 $A`" = '/*! This file is auto-generated */' ]
		then	echo ""
			echo -n "DELETE TOP LINE $A ,	"
			$AWK 'NR==1{}NR==eof{exit}NR==2{printf("%s",$0)}NR>2{printf("\n%s",$0)}' "$A" > $A$$
			A_=`ls --full-time "$A" | $AWK '{printf("%s %s",$6,$7)}'`
			rm -f "$A"
			mv "$A$$" "$A"
			touch -d "$A_" "$A"
			echo "TOUCHED !"
			if [ -f "${A}.gz" ]
			then	echo "	REMOVED	${A}.gz"
				rm -f "${A}.gz"
			fi
		fi
	done
	for A in `grep -v "^0" "${LOGDIR}/COM_CSS_WC.TXT" | sed 's/[^ ]* //'`
	do	echo -n "<"
		if [ "`head -1 $A`" = '/*! This file is auto-generated */' ]
		then	echo ""
			echo -n "DELETE TOP LINE $A ,	"
			$AWK 'NR==1{}NR==eof{exit}NR==2{printf("%s",$0)}NR>2{printf("\n%s",$0)}' "$A" > $A$$
			A_=`ls --full-time "$A" | $AWK '{printf("%s %s",$6,$7)}'`
			rm -f "$A"
			mv "$A$$" "$A"
			touch -d "$A_" "$A"
			echo "TOUCHED !"
			if [ -f "${A}.gz" ]
			then	echo "	REMOVED	${A}.gz"
				rm -f "${A}.gz"
			fi
		fi
	done
	if [ `grep -v "^0" "${LOGDIR}/COM_JS_WC.TXT" | wc -l` -gt 0 ]
	then	echo -n "Remaking ${LOGDIR}/COM_JS_WC.TXT"
		echo -n "" > "${LOGDIR}/COM_JS_WC.TXT"
		for A in `find -type f -name "*[\.\-]min.js" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
		do	wc -l "$A" >> "${LOGDIR}/COM_JS_WC.TXT"
		done
		echo "	done"
	fi
	if [ `grep -v "^0" "${LOGDIR}/COM_CSS_WC.TXT" | wc -l` -gt 0 ]
	then	echo -n "Remaking ${LOGDIR}/COM_CSS_WC.TXT"
		echo -n "" > "${LOGDIR}/COM_CSS_WC.TXT"
		for A in `find -type f -name "*[\.\-]min.css" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
		do	wc -l "$A" >> "${LOGDIR}/COM_CSS_WC.TXT"
		done
		echo "	done"
	fi
	for A in `find -type f -name "*min.js" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH"`
	do	echo -n "."
		if [ "`grep '/\*! This file is auto-generated \*/' "$A"`" != "" ]
		then	echo "
FOUND	$A	/*! This file is auto-generated */	"
			sed "s%/\*! This file is auto-generated \*/%%g" "$A" > $A$$
			rm -f "$A"
			mv "$A$$" "$A"
			if [ -f "${A}.gz" ]
			then	echo "	REMOVED	${A}.gz"
				rm -f "${A}.gz"
			fi
		fi
	done
	echo ""
	for A in `find -type f -name "*min.css" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH"`
	do	echo -n "-"
		if [ "`grep '/\*! This file is auto-generated \*/' "$A"`" != "" ]
		then	echo "
FOUND	$A	/*! This file is auto-generated */	"
			sed "s%/\*! This file is auto-generated \*/%%g" "$A" > $A$$
			rm -f "$A"
			mv "$A$$" "$A"
			if [ -f "${A}.gz" ]
			then	echo "	REMOVED	${A}.gz"
				rm -f "${A}.gz"
			fi
		fi
	done
	echo ""
fi

echo $LINE
yn=""
while [ "$yn" != "n" -a "$yn" != "y" ]
do	echo -n "050 Q : Do you want to have a process looking for MD5 ? (y/n) : "
	read yn
done
if [ "$yn" != "n" ]
then
	echo 'for N in `find -type f -name "*\.min\.js" | grep -E \.min\.js$ | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`'
	echo "		------"
	for N in `find -type f -name "*\.min\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	NEWNAME=`echo $N | sed 's/\.min\.js$/.js/'`
		if [ ! -f "$NEWNAME" ]
		then	if [ ! -s "${N}.MD5" ]
			then	GETNUM
				echo "$NUM2019 : Making ${N}.MD5" | tee -a "$LOGFILE12"
				md5sum "$N" | tee "${N}.MD5"
				cat "${N}.MD5" >> "$MD5TABLEFILE"
				if [ "`fgrep -f "${N}.MD5" "${MD5TABLEFILE2}"`" = "" ]
				then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
				fi
			fi
			MAKEGZ "${N}"
		fi
	done
	GETNUM -1
	echo 'for N in `find -type f -name "\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v \.min\.js$ | sort -f`'
	echo "		------"
	for N in `find -type f -name "\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v \.min\.js$ | sort -f`
	do	if [ ! -s "${N}.MD5" ]
		then	GETNUM
			echo "$NUM2019 : Making ${N}.MD5" | tee -a "$LOGFILE12"
			md5sum "$N" | tee "${N}.MD5"
			cat "${N}.MD5" >> "$MD5TABLEFILE"
			if [ "`fgrep -f "${N}.MD5" "${MD5TABLEFILE2}"`" = "" ]
			then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
			fi
		fi
	done
	GETNUM -1
	echo 'for N in `find -type f -name "*\.min\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`'
	echo "		------"
	for N in `find -type f -name "*\.min\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	NEWNAME=`echo $N | sed 's/\.min\.css$/.css/'`
		if [ ! -f "$NEWNAME" ]
		then	if [ ! -s "${N}.MD5" ]
			then	GETNUM
				echo "$NUM2019 : Making ${N}.MD5" | tee -a "$LOGFILE13"
				md5sum "$N" | tee "${N}.MD5"
				cat "${N}.MD5" >> "$MD5TABLEFILE"
				if [ "`fgrep -f "${N}.MD5" "${MD5TABLEFILE2}"`" = "" ]
				then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
				fi
			fi
			MAKEGZ "${N}"
		fi
	done
	GETNUM -1
	echo 'for N in `find -type f -name "\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v \.min\.css$ | sort -f`'
	echo "		------"
	for N in `find -type f -name "\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v \.min\.css$ | sort -f`
	do	if [ ! -s "${N}.MD5" ]
		then	GETNUM
			echo "$NUM2019 : Making ${N}.MD5" | tee -a "$LOGFILE13"
			md5sum "$N" | tee "${N}.MD5"
			cat "${N}.MD5" >> "$MD5TABLEFILE"
			if [ "`fgrep -f "${N}.MD5" "${MD5TABLEFILE2}"`" = "" ]
			then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
			fi
		fi
	done
	NUM2019=0
	for N in `find -type f -name "*.MD5" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E "\.MD5$" | sort -f`
	do	ORGFILE=`echo $N | sed 's/\.MD5$//'`
		if [ -f "$ORGFILE" ]
		then	NEWER=`find "$ORGFILE" -newer "$N"`
			if [ $? -ne 0 ] ; then echo "ERROR NEWER 001" ; exit ; fi
			if [ "$NEWER" = "" ]
			then	GETDATETIME "$ORGFILE"
				ls --full-time "$ORGFILE" | $AWK '{printf("> %s %s %s\n",$6,$7,$9)}' | sed 's/\.0*//'
				touch -d "$A_" "${N}"
				if [ $? -ne 0 ] ; then echo "ERROR TOUCH 001" ; exit ; fi
				ls --full-time "$N" | $AWK '{printf("  %s %s %s\n",$6,$7,$9)}' | sed 's/\.0*//'
				if [ -f "${ORGFILE}.gz" ]
				then	NEWER=`find "${ORGFILE}.gz" -newer "$N"`
					if [ $? -ne 0 ] ; then echo "ERROR NEWER 002" ; exit ; fi
					if [ "$NEWER" = "" ]
					then	touch -d "$A_" "${ORGFILE}.gz"
						if [ $? -ne 0 ] ; then echo "ERROR TOUCH 002" ; exit ; fi
					fi
					ls --full-time "${ORGFILE}.gz" | $AWK '{printf(" |%s %s %s\n",$6,$7,$9)}' | sed 's/\.0*//'
				fi
			fi
		fi
	done
fi
echo -n "060 Q : Do you want to have a process looking for js ? (y/n) [y] : "
read yn
if [ "$yn" != "n" ]
then	NUM2019=0
	for N in `find -type f -name "*[\.\-]min\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
	do	NUM2019=`expr $NUM2019 + 1`
		echo ""
		echo "$NUM2019 :	${N}"
		JUDGE2019=`echo "$N" | sed 's/[\.\-]min\.js$/.js/'`
		if [ $? -ne 0 ] ; then echo "ERROR A00" ; exit ; fi
		echo -n "	$JUDGE2019 "
		if [ ! -f "$JUDGE2019" ]
		then	if [ ! -s "${N}.MD5" ]
			then	md5sum "$N" | tee "${N}.MD5"
				if [ $? -ne 0 ] ; then echo "ERROR A01" ; exit ; fi
				cat "${N}.MD5" >> "${MD5TABLEFILE}"
				if [ $? -ne 0 ] ; then echo "ERROR A02" ; exit ; fi
				if [ "`fgrep -f "${N}.MD5" "${MD5TABLEFILE2}"`" = "" ]
				then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
				fi
			else	NEWER=`find "${N}.MD5" -newer "$N"`
				if [ $? -ne 0 ] ; then echo "ERROR A03" ; exit ; fi
				if [ "$NEWER" = "" ]
				then	echo -n "$NUM2019 : "
					echo "ERROR TIMESTAMP ${JUDGE2019}.MD5 !" | tee -a "${LOGFILE3}"
				fi
			fi
		fi
	done
	NUM2019=0
	for N in `find -type f -name "*.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E "\.js$" | grep -E -v "\.min\.js$" | sort -f`
	do	if [ ! -s "${N}.MD5" ]
		then	if [ "$T1ST" != "y" ]
			then	JUDGE2019=`fgrep "${N}" "${MD5TABLEFILE2}" | tail -1`
				if [ "$JUDGE2019" != "" ]
				then	echo $JUDGE2019 | md5sum -c
					if [ $? -eq 0 ]
					then	md5sum "$N" | tee "${N}.MD5"
						if [ $? -ne 0 ] ; then echo "ERROR 011" ; exit ; fi
					fi
				else
					md5sum "$N" | tee "${N}.MD5"
					if [ $? -ne 0 ] ; then echo "ERROR 012" ; exit ; fi
				fi
			else	md5sum "$N" | tee "${N}.MD5"
				cat "${N}.MD5" >> "${MD5TABLEFILE}"
			fi
			GETDATETIME "$N"
			if [ $? -ne 0 ] ; then echo "ERROR GETDATETIME 012" ; exit ; fi
			if [ "$OK_" = "n" ] ; then echo "ERROR GETDATETIME 901" ; exit ; fi
			touch -d "$A_" "${N}.MD5"
			if [ $? -ne 0 ] ; then echo "ERROR GETDATETIME 112" ; exit ; fi
			if [ "$T1ST" != "y" ]
			then	JUDGE2019=`fgrep -f "${N}" "${MD5TABLEFILE2}"`
				if [ "$JUDGE2019" = "" ]
				then
					cat "${N}.MD5" >> "${MD5TABLEFILE2}"
				fi
			fi
		else	B_=`md5sum -c "${N}.MD5" | grep OK$`
			if [ "$B_" = "" ]
			then	echo ""
				echo "ERROR MD5 ##### ${N}.MD5"  | tee -a "${LOGFILE3}"
			else	echo -n "."
			fi
			JUDGE2019=`fgrep "${N}" "${MD5TABLEFILE2}"`
			if [ "$JUDGE2019" = "" ]
			then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
			fi
		fi
		MINNAME=`echo $N | sed 's/\.js$//'`
		if [ ! -f "${MINNAME}.min.js" ]
		then	NUM2019=`expr $NUM2019 + 1`
			NUM2019A=`echo $NUM2019 | $AWK '{printf("%04d: ",$1)}'`
			echo ""
			echo -n "${NUM2019A} " | tee -a "${LOGFILE3}"
			echo "${MINNAME}.min.js NOTFOUND for $N" | tee -a "${LOGFILE3}"
			if [ $? -ne 0 ] ; then echo "ERROR 013" ; exit ; fi
		fi
	done
	if [ -f "${LOGFILE3}" ] ; then touch -t $STAMP "${LOGFILE3}" ; fi
	if [ -s "${LOGFILE3}" ]
	then	touch -t $STAMP "${LOGFILE3}"
		echo "	----------"
		yn=""
		while [ "$yn" != "n" -a "$yn" != "y" ]
		do	echo -n "065 Q : Exit & Check the file \"${LOGFILE3}\" NOW ? (y/n) : "
			read yn
		done
		if [ "$yn" != "n" ] ; then exit ; fi
	fi
	echo ""
fi

echo -n "070 Q : Do you want to have a process looking for css ? (y/n) [y] : "
read yn
if [ "$yn" != "n" ]
then	NUM2019=`grep -E \.css$ "${MD5TABLEFILE2}"`
	if [ "$NUM2019" = "" ] ; then T1ST="y" ; fi
	NUM2019=0
	for N in `find -type f -name "*.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E "\.css$" | grep -E -v "\.min\.css$"| sort -f`
	do	if [ ! -s "${N}.MD5" ]
		then	if [ "$T1ST" != "y" ]
			then	JUDGE2019=`fgrep "${N}" "${MD5TABLEFILE2}" | tail -1`
				if [ "$JUDGE2019" != "" ]
				then	echo $JUDGE2019 | md5sum -c
					if [ $? -eq 0 ]
					then	md5sum "$N" | tee "${N}.MD5"
						if [ $? -ne 0 ] ; then echo "ERROR 021" ; exit ; fi
					fi
				else
					md5sum "$N" | tee "${N}.MD5"
					if [ $? -ne 0 ] ; then echo "ERROR 022" ; exit ; fi
				fi
			else	md5sum "$N" | tee "${N}.MD5"
				cat "${N}.MD5" >> "${MD5TABLEFILE}"
			fi
			GETDATETIME "$N"
			if [ $? -ne 0 ] ; then echo "ERROR GETDATETIME 022" ; exit ; fi
			if [ "$OK_" = "n" ] ; then echo "ERROR GETDATETIME 902" ; exit ; fi
			touch -d "$A_" "${N}.MD5"
			if [ $? -ne 0 ] ; then echo "ERROR GETDATETIME 122" ; exit ; fi
			if [ "$T1ST" != "y" ]
			then	JUDGE2019=`fgrep -f "${N}" "${MD5TABLEFILE2}"`
				if [ "$JUDGE2019" = "" ]
				then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
				fi
			fi
		else	B_=`md5sum -c "${N}.MD5" | grep OK$`
			if [ "$B_" = "" ]
			then	echo ""
				echo "ERROR MD5 ##### ${N}.MD5"  | tee -a "${LOGFILE4}"
			else	echo -n "."
			fi
			JUDGE2019=`fgrep "${N}" "${MD5TABLEFILE2}"`
			if [ "$JUDGE2019" = "" ]
			then	cat "${N}.MD5" >> "${MD5TABLEFILE2}"
			fi
		fi
		MINNAME=`echo $N | sed 's/\.css$//'`
		if [ ! -f "${MINNAME}.min.css" ]
		then	NUM2019=`expr $NUM2019 + 1`
			NUM2019A=`echo $NUM2019 | $AWK '{printf("%04d: ",$1)}'`
			echo ""
			echo -n "${NUM2019A} " | tee -a "${LOGFILE4}"
			echo "${MINNAME}.min.css NOTFOUND for $N" | tee -a "${LOGFILE4}"
			if [ $? -ne 0 ] ; then echo "ERROR 023" ; exit ; fi
		fi
	done
	if [ -f "${LOGFILE4}" ] ; then touch -t $STAMP "${LOGFILE4}" ; fi
	if [ -s "${LOGFILE4}" ]
	then	touch -t $STAMP "${LOGFILE4}"
		echo "	----------"
		yn=""
		while [ "$yn" != "n" -a "$yn" != "y" ]
		do	echo -n "075 Q : Exit & Check the file \"${LOGFILE4}\" NOW ? (y/n) : "
			read yn
		done

		if [ "$yn" != "n" ] ; then exit ; fi
	fi
	echo ""
fi
if [ -s "${MD5TABLEFILE}" ] ;  then touch -t $STAMP "${MD5TABLEFILE}"  ; fi
if [ -s "${MD5TABLEFILE2}" ] ; then touch -t $STAMP "${MD5TABLEFILE2}" ; fi


echo -n "080 Q : Do you want to have a MAIN process ? (y/n) [y] : "
read yn
if [ "$yn" != "n" ] ; then
NUM2019=0
for N in `find -type f -name "*[\.\-]min\.*" | grep -E -v "\.php$" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v "\.gz$" | grep -E -v "\.MD5$" | sort -f`
do	NUM2019=`expr $NUM2019 + 1`
	NUM2019A=`echo $NUM2019 | $AWK '{printf("%04d: ",$1)}'`
	echo -n "${NUM2019A} "
	ORGFILE=`echo $N | sed 's/[\.\-]min\././'`
	GZFILE="${N}.gz"
	if [ -f "${GZFILE}" ]
	then	echo -n "${GZFILE}	CHECKING MD5	"
		N__MD5=`md5sum "${N}" | awk '{printf("[%s]",$1)}'`
		GZ_MD5=`gzip -dc "${GZFILE}" | md5sum | awk '{printf("[%s]",$1)}'`
		if [ "$N__MD5" != "$GZ_MD5" ]
		then	echo -n "${NUM2019A}" | tee -a "${LOGFILE2}"
			echo -n "MD5 $N__MD5 $GZ_MD5" | tee -a "${LOGFILE2}"
			rm -f "${GZFILE}"
			echo "REMOVED ${GZFILE} because diferent MD5 !!	" | tee -a "${LOGFILE2}"
		else	echo "SAME MD5 !!"
		fi
	fi
	if [ -f "$ORGFILE" ]
	then	N__TIME=`date -r "$N" +%s`
		ORGTIME=`date -r "$ORGFILE" +%s`
		DIFFERENCE=`expr ${N__TIME} - ${ORGTIME}`
		if [ $DIFFERENCE -lt 0 ]
		then
			echo " \"${ORGFILE}\" is newer than \"${N}\" !" | tee -a "${LOGFILE2}"
			echo -n "\$N__TIME:${N__TIME}	" | tee -a "${LOGFILE2}"
			echo "\$ORGTIME:${ORGTIME}" | tee -a "${LOGFILE2}"
			ls --full-time "$ORGFILE" | tee -a "${LOGFILE2}"
			ls --full-time "$N" | tee -a "${LOGFILE2}"
		else	GETDATETIME "$ORGFILE"
			if [ "$OK_" = "n" ] ; then echo "ERROR GETDATETIME 001" ; exit ; fi
			touch -d "$A_" "$N"
		fi
	else	echo "NOT FOUND A FILE \"${ORGFILE}\" for \"${N}\" !" | tee -a "${LOGFILE}"
	fi
	ORGSIZE=`ls -l "$N"	 | $AWK '{print $5}'`
	if [ -f "${GZFILE}" ]
	then	NEWER=`find "$N" -newer "$GZFILE"`
		if [ "$NEWER" != "" ]
		then	\rm -f "$GZFILE"
			if [ $? -ne 0 ] ; then echo "ERROR 032" ; exit ; fi
			echo "REMOVED $GZFILE because $N -newer $GZFILE ." | tee -a "${LOGFILE2}"
		else	ORGSIZE=`ls -l "$N"	 | $AWK '{print $5}'`
			GZSIZE=`ls -l "$GZFILE"	 | $AWK '{print $5}'`
#			if [ $ORGSIZE -le 1000 ]
			if [ $GZSIZE -gt $ORGSIZE ]
			then	echo ""  | tee -a "${LOGFILE}"
				ls -l $N | tee -a "${LOGFILE}"
				ls -l $GZFILE | tee -a "${LOGFILE}"
				\rm -f "$GZFILE"
				if [ $? -ne 0 ] ; then echo "ERROR 033" ; exit ; fi
#				echo "REMOVED $GZFILE because a size -le 1000"
				echo "REMOVED $GZFILE because gzfile size -gt org file" | tee -a "${LOGFILE}"
			fi
		fi
	fi
#	if [ $ORGSIZE -gt 1000 ]
#	then
		if [ ! -f "$GZFILE" ]
		then	echo "=====	gzip -9 -f -k -v $N"
			gzip -9 -f -k -v "$N"
			if [ $? -ne 0 ] ; then echo "ERROR gzip 134" ; exit ; fi
			echo -n $NUM2019A
			echo -n "> MADE GZ"
		else	echo -n $NUM2019A
			echo -n "< KEPT GZ"
		fi
		echo -n " $N" | tee -a "${LOGFILE}"
		NEWSIZE=`ls -l "$GZFILE" | $AWK '{print $5}'`
		if [ $? -ne 0 ] ; then echo "ERROR 035" ; exit ; fi
		if [ $ORGSIZE -gt 0 ]
		then	MGNFIC=`expr $NEWSIZE \* 100`
			MGNFIC=`expr $MGNFIC / $ORGSIZE`
			if [ $? -ne 0 ] ; then echo "ERROR 036" ; exit ; fi
		else	MGNFIC="-"
		fi
		echo -n "	${NEWSIZE}/${ORGSIZE}(${MGNFIC}%)" | tee -a "${LOGFILE}"
		if [ $? -ne 0 ] ; then echo "ERROR 037" ; exit ; fi
		GZSIZE=`ls -l "$GZFILE"	 | $AWK '{print $5}'`
#		if [ $ORGSIZE -lt 1000 ]
		if [ $GZSIZE -gt $ORGSIZE ]
		then	echo ""  | tee -a "${LOGFILE}"
			ls -l $N | tee -a "${LOGFILE}"
			ls -l $GZFILE | tee -a "${LOGFILE}"
			\rm -f "$GZFILE"
			if [ $? -ne 0 ] ; then echo "ERROR 038" ; exit ; fi
			echo "REMOVED $GZFILE because gzfile size -gt org file" | tee -a "${LOGFILE}"
		else	if [ $MGNFIC -gt $REMOVEPERCENT ]
			then	\rm -f "$GZFILE"
				if [ $? -ne 0 ] ; then echo "ERROR 039" ; exit ; fi
				echo " REMOVED! -lt ${REMOVEPERCENT}%" | tee -a "${LOGFILE}"
			else	echo "" | tee -a "${LOGFILE}"
			fi
		fi
#	fi
	if [ -f "$GZFILE" ]
	then	GETDATETIME "$N"
		if [ "$OK_" = "n" ] ; then echo "ERROR GETDATETIME 002" ; exit ; fi
		touch -d "$A_" "$GZFILE"
	fi
done
fi

echo -n "090 Q : Do you want to touch all of .gz files ? (y/n) [y] : "
read yn
if [ "$yn" != "n" ]
then	NUM2019=0
	for N in `find -type f -name "*.gz" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E "\.(css|js)\.gz$"`
	do	NUM2019=`expr $NUM2019 + 1`
		NUM2019A=`echo $NUM2019 | $AWK '{printf("%04d: ",$1)}'`
		echo "${NUM2019A} : ----------"
		OM=`echo "$N" | sed 's/\.gz$//'`
		if [ -f "$OM" ]
		then	GETDATETIME "$OM"
			if [ "$OK_" = "n" ] ; then echo "ERROR GETDATETIME 003" ; exit ; fi
			touch -d "$A_" "$N"
			ls --full-time "$OM" | $AWK '{printf("  %s %s %s\n",$6,$7,$9)}' | sed 's/\.0*//'
			ls --full-time "$N" | $AWK '{printf("  %s %s %s\n",$6,$7,$9)}' | sed 's/\.0*//'
		fi
	done
fi

touch -t $STAMP "${LOGFILE}"
if [ -f "${LOGFILE2}" ] ; then touch -t $STAMP "${LOGFILE2}" ; fi
if [ -s "${LOGFILE12}" ] ;  then touch -t $STAMP "${LOGFILE12}"  ; fi
if [ -s "${LOGFILE13}" ] ;  then touch -t $STAMP "${LOGFILE13}"  ; fi
if [ -s "${LOGFILE14}" ] ;  then touch -t $STAMP "${LOGFILE14}"  ; fi


echo $LINE
echo $VERSION
echo "START :	$BEGINT"
echo -n 'find -type f -name "Thumbs.db" -exec \rm -f "{}" \;	'
find -type f -name "Thumbs.db" -exec \rm -f "{}" \;
echo "Done."
echo "SIZE ZERO MD5 FILES ....."
find -type f -name "*.MD5" -size 0 -exec ls -lh "{}" \;
if [ `find -type f -name "*.MD5" -size 0 -print | wc -l` -gt 0 ]
then	echo -n "REMOVED $MD5TABLEFILE2 ....."
	rm -f "${MD5TABLEFILE2}" ; touch -t 202001010000 "${MD5TABLEFILE2}"
	echo " done."
fi
echo -n "ERASING SIZE ZERO MD5 FILES ....."
find -type f -name "*.MD5" -size 0 -exec rm -f "{}" \;
echo " done."
echo "
FINISH:	`date`"
exit
Version:2.3 2022/11/04
+ for N in `find -type f -name "*[\.\-]min\.*" | grep -E -v "\.php$" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v "\.gz$" | grep -E -v "\.MD5$" | sort -f`
+ if [ -f "${GZFILE}" ]
  then	echo -n "${GZFILE}	CHECKING MD5	"
	N__MD5=`md5sum "${N}" | awk '{printf("[%s]",$1)}'`
	GZ_MD5=`gzip -dc "${GZFILE}" | md5sum | awk '{printf("[%s]",$1)}'`
	echo "done"
	if [ "$N__MD5" != "$GZ_MD5" ]
	then	echo MD5 $N__MD5 $GZ_MD5
		rm -f "${GZFILE}"
		echo " REMOVED ${GZFILE} because diferent MD5 !!"
	fi
  fi

Version:2.2a 2022/08/31
+ echo -n "" > "${LOGDIR}/COM_JS_WC.TXT"
+ echo -n "" > "${LOGDIR}/COM_CSS_WC.TXT"

Version:2.2 2022/08/11
- for A in `find -type f -name "*min.js" -print`
+ for A in `find -type f -name "*min.js" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH"`
- for A in `find -type f -name "*min.css" -print`
+ for A in `find -type f -name "*min.css" -print | fgrep -vf "${LOGDIR}/.IGNOREPATH"`

Version:2.1a 2022/08/05
+ then	echo "/z  Sources4PHP/" > "${LOGDIR}/.IGNOREPATH"

Version:2.1 2022/07/15
  echo -n "040 Q : DELETE TOP LINE /*! This file is auto-generated */ ? (y/n) [n] : "
+ 	for A in `find -type f -name "*min.js" -print`
+	do	echo -n "-"
+		if [ "`grep '/\*! This file is auto-generated \*/' "$A"`" != "" ]
+		then	sed "s%/\*! This file is auto-generated \*/%%g" "$A" > $A$$
+			A_=`ls --full-time "$A" | $AWK '{printf("%s %s",$6,$7)}'`
+			rm -f "$A"
+			mv "$A$$" "$A"
+			touch -d "$A_" "$A"
+			if [ -f "${A}.gz" ]
+			then	echo "	REMOVED	${A}.gz"
+				rm -f "${A}.gz"
+			fi
+		fi
+	done
+	echo ""
+	for A in `find -type f -name "*min.css" -print`
+	do	echo -n "-"
+		if [ "`grep '/\*! This file is auto-generated \*/' "$A"`" != "" ]
+		then	sed "s%/\*! This file is auto-generated \*/%%g" "$A" > $A$$
+			A_=`ls --full-time "$A" | $AWK '{printf("%s %s",$6,$7)}'`
+			rm -f "$A"
+			mv "$A$$" "$A"
+			touch -d "$A_" "$A"
+			if [ -f "${A}.gz" ]
+			then	echo "	REMOVED	${A}.gz"
+				rm -f "${A}.gz"
+			fi
+		fi
+	done
+	echo ""


Version:2.0b 2021/09/04
+ yn=""
+ while [ "$yn" != "n" -a "$yn" != "y" ]
+ do	echo -n "050 Q : Do you want to have a process looking for MD5 ? (y/n) : "
	read yn
+ done


Version:2.0a 2021/05/21
- echo -n "020 Q : If there is no min.js|min.css file, delete min.js.gz|min.css.gz file ? (y/n) [n] : "
+ echo -n "010 Q : If there is no min.js|min.css file, delete min.js.gz|min.css.gz file ? (y/n) [n] : "
- echo -n "030 Q : If there is no .js|.css source file, copy from .min. file ? (y/n) [n] : "
+ echo -n "020 Q : If there is no .js|.css source file, copy from .min. file ? (y/n) [n] : "
- echo -n "040 Q : Do you want to REMAKE & RETOUCH MD5 at ${MAINTENANCEOFMISSINGDIR}/${DN} ? (y/n) [n] : "
+ echo -n "030 Q : Do you want to REMAKE & RETOUCH MD5 at ${MAINTENANCEOFMISSINGDIR}/${DN} ? (y/n) [n] : "
- echo -n "010 Q : DELETE TOP LINE /*! This file is auto-generated */ ? (y/n) [n] : "
+ echo -n "040 Q : DELETE TOP LINE /*! This file is auto-generated */ ? (y/n) [n] : "

Version:2.0 2021/03/10
+ 010 Q : DELETE TOP LINE /*! This file is auto-generated */ ? (y/n) [n] :
+ 030 Q : If there is no .js|.css source file, copy from .min. file ? (y/n) [n] :
	rename .MD5 to .md5

Version:1.9 2020/09/06
+ Q : If there is no min.js|min.css file, delete min.js.gz|min.css.gz file ? (y/n) [n] :
- -name "*[-\.]min\.js"
+ -name "*[\-\.]min\.js"
+	OTHERNAME=`echo $ORGNAME | sed 's/\.js$/.min.js/'`
+	if [ ! -f "$OTHERNAME" ]
	then	cp -pv "$N" "$OTHERNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
	fi
- -name "*[-\.]min\.css"
+ -name "*[\-\.]min\.css"
+	OTHERNAME=`echo $ORGNAME | sed 's/\.css$/.min.css/'`
+	if [ ! -f "$OTHERNAME" ]
	then	cp -pv "$N" "$OTHERNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
	fi
- -name "*\.min\.*"
+ -name "*[\.\-]min\.*"

Version:1.8c 2020/08/14
- echo "/wp-content/mmr/" > "${LOGDIR}/.IGNOREPATH"
+ echo "/wp-content/" > "${LOGDIR}/.IGNOREPATH"
+ echo -n "CHECKING SIZE ZERO MD5 FILES ....."
- Q : Exit & Check the file \"${LOGDIR}/COM_CSS_WC.TXT\" & \"${LOGDIR}/COM_CSS_WC.TXT\" NOW ? (y/n) [y] :
+ Q : Exit & Check the file \"${LOGDIR}/COM_CSS_WC.TXT\" & \"${LOGDIR}/COM_CSS_WC.TXT\" NOW ? (y/n) [n] :
- echo -n "CHECKING SIZE ZERO MD5 FILES ....."
- echo -n "${LINE}   Please Enter!"
+ echo -n "${LINE}   Please Enter!	KEY \"e\" Enter to SKIP.	"


Version:1.8b 2020/07/29
+ Q : Exit & Check the file \"${LOGDIR}/COM_CSS_WC.TXT\" & \"${LOGDIR}/COM_CSS_WC.TXT\" NOW ? (y/n) [y] :

Version:1.8a
- else	echo "!!! NOTHING $N"
	echo -n "${NUM_}D: "
	ls -l "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.MD5" | sed 's%/\./%/%'
	echo -n "${LINE}   Please Enter!"
	read yn
  fi
 done
+ 	echo -n "${NUM_}D: "
	ls -l "${MAINTENANCEOFMISSINGDIR}/${DN}/${NN}.MD5" | sed 's%/\./%/%'
	echo -n "${LINE}   Please Enter!"
	read yn
  else	echo "!!! NOTHING $N"
  fi
 done

Version:1.8 2020/05/27
+ for A in `find -type f -name "*.min.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH"`
  do	wc -l "$A" >> ${LOGDIR}/COM_JS_WC.TXT
  done
+ for A in `find -type f -name "*.min.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH"`
  do	wc -l "$A" >> ${LOGDIR}/COM_CSS_WC.TXT
  done

Version:1.7a 2020/05/06
- else	echo "WARNING NOTHING ${OTHER}.gz"

Version:1.7 2020/04/10
- REMOVEPERCENT=70
+ REMOVEPERCENT=95
+ GZSIZE=`ls -l "$GZFILE"	 | $AWK '{print $5}'`
- if [ $ORGSIZE -le 1000 ]
+ if [ $GZSIZE -gt $ORGSIZE ]
- echo "REMOVED $GZFILE because a size -le 1000"
+ echo "REMOVED $GZFILE because gzfile size -gt org file"
- echo " REMOVED!" | tee -a "${LOGFILE}"
+ echo " REMOVED! -lt ${REMOVEPERCENT}%" | tee -a "${LOGFILE}"
- else	md5sum -c "${N}.MD5"
  if [ $? -ne 0 ]
+ else	B_=`md5sum -c "${N}.MD5" | grep OK$`
  if [ "$B_" = "" ]

Version:1.6 2020/04/05
- echo -n " Q : Do you want to make .js | .css from .min source ? (y/n) [y] : "
+ echo -n " Q : If there is no .js|.css source file, copy from .min. file ? (y/n) [n] : "
- echo -n " Q : Do you want to RETOUCH MD5 at ${MAINTENANCEOFMISSINGDIR}/${DN} ? (y/n) [n] : "
+ echo -n " Q : Do you want to REMAKE & RETOUCH MD5 at ${MAINTENANCEOFMISSINGDIR}/${DN} ? (y/n) [n] : "
- if [ "$NEWER" != "" ]
+ N__TIME=`date -r "$N" +%s`
  ORGTIME=`date -r "$ORGFILE" +%s`
  DIFFERENCE=`expr ${N__TIME} - ${ORGTIME}`
  #NEWER=`find "$N" ! -newer "$ORGFILE"`
  #if [ $? -ne 0 ] ; then echo "ERROR 031" ; exit ; fi
  #if [ "$NEWER" != "" -a $DIFFERENCE -ge 0 ]
  if [ $DIFFERENCE -lt 0 ]

Version:1.5d 2020/04/02
+ echo -n " Q : Do you want to RETOUCH MD5 at ${MAINTENANCEOFMISSINGDIR}/${DN} ? (y/n) [n] : "

Version:1.5c 2020/04/01
+ MAINTENANCEOFMISSINGDIR="${LOGDIR}/MOMFS"
+ if [ ! -d "$MAINTENANCEOFMISSINGDIR" ] ; then mkdir "$MAINTENANCEOFMISSINGDIR" ; fi
+ MAINTENANCEOFMISSINGFILES="${LOGDIR}/MOMFS/${STAMP} MOMFS.log"
+ echo "FINDING ..... MOMF.log"

Version:1.5b 2020/03/30
+ MAINTENANCEOFMISSINGFILES="${LOGDIR}/${STAMP} MOMFS.log"
+ cp -pv "$N" "$ORGNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"
+ cp -pv "$N" "$ORGNAME" | tee -a "${MAINTENANCEOFMISSINGFILES}"

Version:1.5 2020/03/28
+ echo -n " Q : Do you want to make .js | .css from .min source ? (y/n) [y] : "

Version:1.4c 2020/03/17
+ echo -n "ERASING SIZE ZERO MD5 FILES ....."
+ find -type f -name "*.MD5" -size 0 -exec rm -f "{}" \;
+ echo " done."
+ if [ `find -type f -name "*.MD5" -size 0 -print | wc -l` -gt 0 ]
+ then	echo -n "REMOVED $MD5TABLEFILE2 ....."
+	rm -f "${MD5TABLEFILE2}"
+	echo " done."
+ fi


Version:1.4b 2020/03/01
- if [ ! -f "${N}.MD5" ]
+ if [ ! -s "${N}.MD5" ]

Version:1.4a 2019/03/12
- else	echo "NOT FOUND A FILE \"${ORGFILE}\" for \"${N}\" !" | tee -a "${LOGFILE2}"
+ else	echo "NOT FOUND A FILE \"${ORGFILE}\" for \"${N}\" !" | tee -a "${LOGFILE}"

Version:1.4 2019/02/04
+ function MAKEGZ()
+ for N in `find -type f -name "*\.min\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
+ for N in `find -type f -name "\.js" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v \.min\.js$ | sort -f`
+ for N in `find -type f -name "*\.min\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | sort -f`
+ for N in `find -type f -name "\.css" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v \.min\.css$ | sort -f`
- for N in `find -type f -name "*\.min\.*" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v "\.gz$" | sort -f`
+ for N in `find -type f -name "*\.min\.*" | fgrep -vf "${LOGDIR}/.IGNOREPATH" | grep -E -v "\.gz$" | grep -E -v "\.MD5$" | sort -f`

Version:1.3 2019/01/16
- JUDGE2019=`fgrep -f "${N}.MD5" "${MD5TABLEFILE2}"`
+ JUDGE2019=`fgrep -f "${N}" "${MD5TABLEFILE2}"`

Version:1.1 2019/01/02
+	cat "${N}.MD5" >> "${MD5TABLEFILE}"
+	cat "${N}.MD5" >> "${LOGDIR}/$$.LOG"
	cat "${LOGDIR}/$$.LOG" >> "${MD5TABLEFILE2}"

Version:1.2 2019/01/04
- touch 201901010000 "${MD5TABLEFILE2}"
+ touch -t 201901010000 "${MD5TABLEFILE2}"
  T1ST="y"
+ if [ "$T1ST" != "y" ]
+ echo -n " Q : Exit & Check the file \"${LOGFILE4}\" NOW ? (y/n) [y] : "
